package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.responsitory.IOrderReponsitory;
import com.devcamp.pizza365.responsitory.IUserReponsitory;

@RestController
@CrossOrigin(value = "*", maxAge = -1) // maxAge = -1 không lưu cache vào máy khách
@RequestMapping("order")
public class COrderController {

    @Autowired
    IOrderReponsitory pOrderReponsitory;

    @Autowired
    IUserReponsitory pCuserRepository;

    // all
    @GetMapping("/all")
    public ResponseEntity<Object> getAllUsers() {
        // ResponseEntity<Object> tạo ra đối tượng responseEntity với kiểu dữ liệu trả
        // về là Object

        List<COrder> userList = new ArrayList<COrder>();
        pOrderReponsitory.findAll().forEach(userList::add);

        // kiểm tra xem list có rỗng hãy không . nếu rỗng thì báo lỗi ..
        if (!userList.isEmpty()) {
            return new ResponseEntity<>(userList, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    // thông tin user theo id
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable long id) {
        Optional<COrder> userFouned = pOrderReponsitory.findById(id);
        // Optional để giải quyết các vấn đề về truy cập vào các đối tượng có thể null.
        // Nó đóng vai trò là một bao bọc an toàn cho các giá trị có thể không tồn tại,
        // giúp tránh những lỗi NullPointerException khi truy cập vào đối tượng null.
        if (userFouned.isPresent()) {
            return new ResponseEntity<>(userFouned, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tạo mới user
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createUser(@PathVariable long id, @RequestBody COrder userFormClient) {
        // tìm kiếm user
        Optional<CUser> searchUser = pCuserRepository.findById(id);
        if (searchUser.isPresent()) {
            try {
                // // khởi tạo 1 đối tượng để lưu kết người dùng cần tọa
                COrder _order = new COrder();
                Date _now = new Date(); // lấy ngày tạo từ hệ thông
                _order.setCreated(_now);
                _order.setUpdated(userFormClient.getUpdated());
                _order.setOrderCode(userFormClient.getOrderCode());
                _order.setPaid(userFormClient.getPaid());
                _order.setPizzaSize(userFormClient.getPizzaSize());
                _order.setPizzaType(userFormClient.getPizzaType());
                _order.setPrice(userFormClient.getPrice());
                _order.setVoucherCode(userFormClient.getVoucherCode());
                _order.setUser(searchUser.get()); // get để chuyển dữ liệu optional thành user
                COrder _save = pOrderReponsitory.save(userFormClient);
                return new ResponseEntity<Object>(_save, HttpStatus.OK);

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to create specified user" + e.getCause().getCause().getMessage());

            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }

    }

    // update
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "id") Long paramId, @RequestBody COrder orderFromClient) {
        System.out.println("dong 1 ");
        System.out.println("id order là " + paramId);
        System.out.println("id user là " + orderFromClient.getUserIdUpdated());
        // tìm  order theo id trên data base
        Optional<COrder> searchOrder = pOrderReponsitory.findById(paramId);  //  tìm order cần update 
        Optional<CUser> searchUser = pCuserRepository.findById(orderFromClient.getUserIdUpdated());//tìm user  theo id 
        System.out.println(searchOrder.get());
        // kiểm tra có null hay k ,, true là khác null
        if (searchOrder.isPresent()) {
            System.out.println("tim thay order " + searchOrder);
            try {
                // get(); là của Optional để lấy giá tri của đối tượng
            COrder _order = searchOrder.get();  // get để chuyển dữ liệu optional thành user
            Date _now = new Date(); // lấy ngày tạo từ hệ thông

            _order.setCreated(_now);
            _order.setUpdated(orderFromClient.getUpdated());
            _order.setOrderCode(orderFromClient.getOrderCode());
            _order.setPaid(orderFromClient.getPaid());
            _order.setPizzaSize(orderFromClient.getPizzaSize());
            _order.setPizzaType(orderFromClient.getPizzaType());
            _order.setPrice(orderFromClient.getPrice());
            _order.setVoucherCode(orderFromClient.getVoucherCode());
            // nếu tìm thấy user thi set lại không thì thôi
            if(searchUser.isPresent()){
                _order.setUser(searchUser.get()); // get để chuyển dữ liệu optional thành user
            }
            COrder _save = pOrderReponsitory.save(_order);
                return new ResponseEntity<>(_save, HttpStatus.OK);

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    // delete user 
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
        Optional<COrder> _userData = pOrderReponsitory.findById(id);
        // nếu khác null
        if (_userData.isPresent()) {
            try {
                pOrderReponsitory.deleteById(id);
                return new ResponseEntity<Object>("đã xóa người dùng có id là  " + id, HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }

        } else {
            return new ResponseEntity<Object>("không tìm thấy user", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
