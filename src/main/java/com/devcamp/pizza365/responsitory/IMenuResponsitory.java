package com.devcamp.pizza365.responsitory;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.CMenu;

public interface IMenuResponsitory extends JpaRepository<CMenu, Long>{
    Optional<CMenu> findById(int id);
    CMenu findBySize(String size);

}
